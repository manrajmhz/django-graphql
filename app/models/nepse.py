from django.db import models

class Nepse(models.Model):
    symbol = models.CharField(max_length=20)
    listed_share = models.BigIntegerField()
    paid_up = models.IntegerField()
    total_paid_up_capital = models.DecimalField(max_digits=20, decimal_places=2)
    market_cap = models.DecimalField(max_digits=20, decimal_places=2)
    date_of_operation = models.DateField()
    ltp = models.DecimalField(max_digits=10, decimal_places=2)
    as_of = models.DateField()

    def __str__(self):
        return self.symbol