from django.db import models

class Intent(models.Model):
    intent_name = models.CharField(max_length=1000)
    response = models.CharField(max_length=1000)

    def __str__(self):
        return self.intent_name
    

class IntentExample(models.Model):
    text = models.CharField(max_length=1000)
    vector = models.CharField(max_length=1000)
    intent = models.ForeignKey(Intent, on_delete=models.CASCADE, related_name="intent_example")

    def __str__(self):
        return self.text