import graphene
from graphene_django import DjangoObjectType #used to change Django object into a format that is readable by GraphQL
from app.models.contact import Contact
from app.models.intent import Intent, IntentExample
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField

from graphene_django.debug import DjangoDebug
from app.models.nepse import Nepse

from app.models.product import Product

# """
#     list_contact: Lists all Contacts
#     read_contact: Get single Contact
# """
# class ContactType(DjangoObjectType):
#     class Meta:
#         model = Contact
#         field = ('id', 'name', 'phone_number')

# class Query(graphene.ObjectType):
#     list_contact = graphene.List(ContactType)
#     read_contact = graphene.Field(ContactType, id=graphene.Int())

#     def resolve_list_contact(root, info):
#         return Contact.objects.all()
    
#     def resolve_read_contact(root, info, id):
#         return Contact.objects.filter(id=id).first()
    

# """
#     create_contact: Create a Contact
# """
# class CreateContactMutation(graphene.Mutation):
#     class Arguments:
#         name = graphene.String()
#         phone_number = graphene.String()
    
#     contact = graphene.Field(ContactType)

#     @classmethod
#     def mutate(cls, root, info, name, phone_number):
#         contact = Contact(name=name, phone_number=phone_number)
#         contact.save()

#         return CreateContactMutation(contact=contact)

# """
#     update_contact: Update details of Contact
# """
# class UpdateContactMutation(graphene.Mutation):
#     class Arguments:
#         id = graphene.ID() # For Update
#         name = graphene.String()
#         phone_number = graphene.String()
    
#     contact = graphene.Field(ContactType)

#     @classmethod
#     def mutate(cls, root, info, name, phone_number, id):

#         contact = Contact.objects.get(id=id)
#         contact.name = name
#         contact.phone_number = phone_number
#         contact.save()

#         return UpdateContactMutation(contact=contact)

# class Mutation(graphene.ObjectType):
#     create_contact = CreateContactMutation.Field()
#     update_contact = UpdateContactMutation.Field()

# schema = graphene.Schema(query=Query, mutation=Mutation)


# class IntentType(DjangoObjectType):
#     class Meta:
#         model = Intent
#         fields = ("id", "intent_name", "response", "intent_example")

# class IntentExampleType(DjangoObjectType):
#     class Meta:
#         model = IntentExample
#         fields = ("id", "text", "vector", "intent")

# class Query(graphene.ObjectType):
#     intent = graphene.List(IntentType)
#     intent_example = graphene.List(IntentExampleType)

#     def resolve_intent(root, info):
#         return Intent.objects.all()
    
#     def resolve_intent_example(root, info):
#         return IntentExample.objects.all()

# schema = graphene.Schema(query=Query)
# schema = graphene.Schema(query=Query, mutation=Mutation)


# Relay

# class IntentNode(DjangoObjectType):
#     class Meta:
#         model = Intent
#         filter_fields = ['intent_name', 'response']
#         interfaces = (relay.Node, )

# class IntentExampleNode(DjangoObjectType):
#     class Meta:
#         model = IntentExample
#         filter_fields = {
#             'text': ['exact', 'icontains', 'istartswith'],
#             'vector': ['exact', 'icontains'],
#             'intent': ['exact'],
#             'intent__intent_name': ['exact'],
#         }

#         interfaces = (relay.Node, )

# class Query(graphene.ObjectType):
#     intent = relay.Node.Field(IntentNode)
#     all_intent = DjangoFilterConnectionField(IntentNode)

# schema = graphene.Schema(query=Query)


# Group By and Aggregate

# from django.db.models import Avg, Sum, Count

# class ProductNode(DjangoObjectType):
#     class Meta:
#         model = Product
#         filter_fields = ['name', 'category']
#         interfaces = (relay.Node, )

# class ProductConnection(relay.Connection):
#     class Meta:
#         node = ProductNode

#     total_count = graphene.Int()
#     average_price = graphene.Float()
#     total_price = graphene.Float()

#     def resolve_total_count(self, info, **kwargs):
#         print(self.__dict__)
#         return self.iterable.count()
    
#     def resolve_average_price(self, info, **kwargs):
#         print(self.iterable.aggregate(avg_price=Avg('price')))
#         return self.iterable.aggregate(avg_price=Avg('price'))['avg_price']
    
#     def resolve_total_price(self, info, **kwargs):
#         return self.iterable.aggregate(total_price=Sum('price'))['total_price']
    
# class Query(graphene.ObjectType):
#     products = DjangoFilterConnectionField(ProductNode)
#     grouped_products = relay.ConnectionField(ProductConnection, category=graphene.String(required=True))

#     def resolve_grouped_products(self, info, category):
#         return Product.objects.filter(category=category)
    
    
# schema = graphene.Schema(query=Query)


# Relay

# class NepseNode(DjangoObjectType):
#     class Meta:
#         model = Nepse
#         filter_fields = ['symbol', 'ltp', 'as_of']
#         interfaces = (relay.Node, )

class NepseNode(DjangoObjectType):
    class Meta:
        model = Nepse
        filter_fields = {
            'symbol': ['exact', 'icontains', 'istartswith'],
            'ltp': ['exact', ],
            'as_of': ['exact', ],
        }

        interfaces = (relay.Node, )

    calculate_market_cap = graphene.Float()

    def resolve_calculate_market_cap(self, info):
        return self.listed_share * self.ltp
    
class Query(graphene.ObjectType):
    nepse = DjangoFilterConnectionField(NepseNode)

schema = graphene.Schema(query=Query)

