from rest_framework import  permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers, status
import csv
from itertools import groupby

from app.models.intent import Intent, IntentExample
from app.models.nepse import Nepse

class ImportCSVView(APIView):
    class InputSerializer(serializers.Serializer):
        csv_file = serializers.FileField()
    
    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        file = serializer.validated_data["csv_file"]
        decoded_file = file.read().decode('utf-8').splitlines()
        read_file = csv.DictReader(decoded_file, delimiter=",")

        for row in read_file:
            symbol = row['Symbol']
            listed_share = row['Listed Share']
            paid_up = row['Paid-up (Rs)']
            total_paid_up_capital = row['Total Paid-up Capital (Rs)']
            market_cap = row['Market Capitalization (Rs)']
            date_of_operation = row['Date of Operation']
            ltp = row['LTP']
            as_of = row['As Of:']
            
            Nepse.objects.create(symbol=symbol, listed_share=listed_share, paid_up=paid_up, 
                                 total_paid_up_capital=total_paid_up_capital, market_cap=market_cap, date_of_operation=date_of_operation, 
                                 ltp=ltp, as_of=as_of)

        return Response(
            {
                "success": True,
            }, 
            status=status.HTTP_201_CREATED
        ) 
    
class ImportCSVViewOld(APIView):
    class InputSerializer(serializers.Serializer):
        csv_file = serializers.FileField()
    
    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        file = serializer.validated_data["csv_file"]
        decoded_file = file.read().decode('utf-8').splitlines()
        read_file = csv.DictReader(decoded_file, delimiter=",")

        all_examples = []
        all_text = []

        for row in read_file:
            all_examples.append(row)
            all_text.append(row["text"])

        all_text_vector = ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']
        
        for index, item in enumerate(all_text_vector):
            all_examples[index]['vector'] = item
            
        final_results = [[*j] for i, j in groupby(sorted(all_examples, key=lambda x: (x['intent_name'])), key=lambda x: (x['intent_name']))]

        print("sorted(all_examples, key=lambda x: (x['intent_name']))")
        print(sorted(all_examples, key=lambda x: (x['intent_name'])))

        for item in final_results:
            for index, i in enumerate(item):
                print(i)
                intent = Intent(intent_name=i['intent_name'], response=i['answer'])
                intent.save()

                intent_example = IntentExample(text=i['text'], vector=i['vector'], intent=intent)
                intent_example.save()


        return Response(
            {
                "success": True,
            }, 
            status=status.HTTP_201_CREATED
        )     